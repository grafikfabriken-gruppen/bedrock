<?php

use GF\Deployer\Recipies\Sandbox;
use GF\Deployer\Recipies\FTP;
use GF\Deployer\Tasks\DeployStandard;
use GF\Deployer\Tasks\BedrockThemeVendors;
use GF\Deployer\Tasks\MDCleanUp;
use GF\Deployer\Tasks\BedrockUploadConstantineThemeNpm;
use function Deployer\after;
use function Deployer\task;
use function Deployer\writeln;
use function Deployer\run;
use GF\Deployer\Tasks\FlushPermalinks;
use GF\Deployer\Tasks\RemoveDbPhp;
use Deployer\Deployer;
use GF\Deployer\Tasks\BedrockUploadThemeGulp;

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('\\GF\\Deployer\\Models\\Example')) {

    if (!file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
        echo __('You need to run composer install first!');
        die();
    }

    require_once $composer;
}


$repository = 'git@gitlab.com:grafikfabriken-gruppen/your-project.git';
$theme_name = 'grafikfabriken';
$staging_deploy_path = '/home/sandbox/your-project.sandbox.grafikfabriken.nu';


// $localhost_deploy_path = '/Applications/XAMPP/htdocs/staging/lokalstationsreklam';
// $ftp_deploy_path = '/Volumes/Samsung_T5/Deployer/FTP/imrab';

/** Localhost deployment */
// new Localhost($repository, $theme_name, $localhost_deploy_path, false);

/** Sandbox deployment */

/** Sandbox deployment */
class SandboxUpdated extends Sandbox
{
    /**
     * Construct
     *
     * @param string $_repository
     * @param string $_theme_name
     * @param string $_deploy_path
     * @param array $_shared_files
     * @param array $_shared_folders
     * @param string $_stage
     */
    public function __construct($_repository, $_theme_name, $_deploy_path, $_use_gulp = false, $_shared_files = array(), $_shared_dirs = array(), $_config_file = null, $_host = null, $_stage = null)
    {
        //Setting up variables
        $this->repository = $_repository;
        $this->theme_name = $_theme_name;
        $this->deploy_path = $_deploy_path;
        $this->config_file = is_null($_config_file) ? $this->config_file : $_config_file;
        $this->host = is_null($_host) ? $this->host : $_host;
        $this->shared_files[] = "web/.htaccess";
        $this->shared_dirs = count($_shared_dirs) > 0 ? $_shared_dirs : $this->shared_dirs;
        $this->use_gulp = $_use_gulp;

        $this->shared_files[] = "web/app/db.php";
        $this->shared_files[] = "web/app/advanced-cache.php";

        //Add deployment task
        DeployStandard::getInstance();

        //Add other tasks!
        new BedrockThemeVendors('install_theme_vendors', $this->host);
        new MDCleanUp('md_clean_up_sandbox', $this->host);

        new BedrockUploadConstantineThemeNpm('theme_upload_npm', $this->host);
        // new RemoveDbPhp('remove_query_monitor', $this->host);


        new FlushPermalinks('flush_permalinks', $this->host);

        //Set host
        $this->set_host();
    }
}

// /** Sandbox deployment */
new SandboxUpdated($repository, $theme_name, $staging_deploy_path, false, array(), array(), null, 'sandbox');
